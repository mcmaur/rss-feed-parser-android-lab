package tk.maurocerbai.iumlabv4_fragment;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import rss.Feed;
import rss.FeedToHTML;
import rssparser.RSSFeedParser;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class ArticleFragment extends Fragment 
{
	public final static String ARG_POSITION = "position";
	private final static String CURRENT_FEED = "currentfeed";
	private String currentFeed;
	int mCurrentPosition=-1;
	private Activity activity;
	private WebView article;
	private ProgressDialog progressDialog;
	
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
	{
		if(savedInstanceState!=null)
		{
			currentFeed=savedInstanceState.getString(CURRENT_FEED);
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
		}
		return inflater.inflate(R.layout.article_view, container, false);
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		this.activity=activity;
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		Bundle args = getArguments();
		if(args != null && args.containsKey(ARG_POSITION))
		{
			updateArticleView (args.getInt(ARG_POSITION));
		}
		else if(mCurrentPosition != -1)
		{
			updateArticleView (mCurrentPosition);
		}
	}
	
	public void updateArticleView (int position)
	{
		article = (WebView) getActivity().findViewById(R.id.article);
		if(!MainActivity.refreshDisplay && currentFeed!=null && position==mCurrentPosition)
			article.loadData(currentFeed, "text/html;charset=utf-8",null);
		else
		{
			if(MainActivity.isConnected())
			{
				progressDialog = ProgressDialog.show(activity, "", "Downloading...");
				new DownloadFeed().execute(Ipsum.Links[position]);
			}
			else
			{
				String noNetwork="No network connection available";
				article.loadData(noNetwork, "text/html;charset=utf-8",null);
			}
		}
		mCurrentPosition = position;
	}
	
	public void onSaveInstanceState (Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putInt(ARG_POSITION,mCurrentPosition);
		outState.putString(CURRENT_FEED,currentFeed);
	}
	
	private class DownloadFeed extends AsyncTask<String, Void, String>
	{
		@Override
		protected String doInBackground(String... urls)
		{
			try
			{
				InputStream is=connectFeed(urls[0]);
				FeedToHTML f2h = new FeedToHTML("Feed I-Learn");
				SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
				boolean pref = sharedPrefs.getBoolean("summaryPref", false);
				if(pref) f2h.setSummary(true);
				Feed feed=downloadFeed(is);
				feed.accept(f2h);
				return f2h.getHTML();
			}
			catch(IOException ioe)
			{
				return ioe.getMessage();
			}
			catch(XmlPullParserException xmlppe)
			{
				return xmlppe.getMessage();
			}
		}
		
		@Override
		protected void onPostExecute(String result)
		{
			progressDialog.dismiss();
			if(article != null)
				currentFeed = result;
				article.loadData(result,"text/html; charset=utf-8",null);
		}
		
		private InputStream connectFeed(String myurl) throws IOException
		{
			InputStream is=null;
			URL url= new URL(myurl);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setReadTimeout(10000);
			conn.setConnectTimeout(15000);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.connect();
			is=conn.getInputStream();
			return is;
		}
	
		private Feed downloadFeed(InputStream is) throws IOException, XmlPullParserException
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xmlParser = factory.newPullParser();	
			xmlParser.setInput(is,null);
			RSSFeedParser parser = new RSSFeedParser(xmlParser);
			Feed feed = parser.rss();
			is.close();
			return feed;
		}
	}
}
