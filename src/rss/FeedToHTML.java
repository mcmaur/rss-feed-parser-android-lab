package rss;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FeedToHTML implements FeedVisitor{
	
	private String title;
	private StringBuilder htmlString;
	private boolean summary=false;
	
	public void setSummary(boolean summary) {
		this.summary = summary;
	}
	
	public FeedToHTML(String title) {
		
		this.title = title;
		this.htmlString = new StringBuilder();
	}
	
	public void visit(FeedMessage feedMessage) {
		htmlString.append("<p><a href=\"");
		htmlString.append(feedMessage.getLink());
		htmlString.append("\"> " + feedMessage.getTitle() + "<a/></p>");
		if(summary) htmlString.append(feedMessage.getDescription());
	}
	
	public void visit(Feed feed) {
		Calendar rightNow = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("MMM dd h:mmmaa");
		htmlString.append("<h3>" + title + " </h3>");
		htmlString.append("<em>Updated: " + formatter.format(rightNow.getTime()) + "</em>");
	}
	
	public String getHTML() {
		
		return htmlString.toString();
	}
}
