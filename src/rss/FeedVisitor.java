package rss;

public interface FeedVisitor {
	
	public void visit(FeedMessage feedMessage);
	public void visit(Feed feed);
}
