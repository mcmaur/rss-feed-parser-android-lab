package rss;

import java.util.*;

public class Feed {
	
	private String title;
	private String link;
	private String description;
	private String pubDate;
	private List<FeedMessage> items = new LinkedList<FeedMessage>();	
		
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public void addFeed(FeedMessage feedMessage) {
		items.add(feedMessage);
	}
	
	/*
	 * accetta un oggetto di tipo FeedVisitor che visita la struttura
	 * */
	public void accept (FeedVisitor visitor) {
		visitor.visit(this);
		
		// for each. per ogni elemento della lista items chiama accept
		for (FeedMessage i : items) {		
			i.accept(visitor);
		}
	}

	@Override
	public String toString() {
		return "Feed [title=" + title + ", link=" + link + ", description="
				+ description + ", pubDate=" + pubDate + ", items=" + items
				+ "]";
	}	
}