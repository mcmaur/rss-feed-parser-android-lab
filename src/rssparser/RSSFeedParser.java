package rssparser;

import java.io.*;
import java.net.*;
import org.xmlpull.v1.*;
import rss.*;

public class RSSFeedParser
{
	private XmlPullParser xmlParser; // lexical analyzer for this parser
	private int eventType;
	private boolean debug;
	private String ns;

	// costruttore
	public RSSFeedParser(XmlPullParser xmlParser) throws XmlPullParserException, IOException 
	{
		this.xmlParser = xmlParser;
		debug = false;
		move("rss0");
	}
	
	//porta avanti il cursore
	protected void move(String dove) throws XmlPullParserException, IOException 
	{
		eventType = xmlParser.next();
		if(debug) System.out.println("Evento: " + XmlPullParser.TYPES[eventType] + ", letto tag (" + dove + ") :" + xmlParser.getName());
	}

	//legge il tag START_TAG
	protected void matchStart(String t) throws XmlPullParserException, IOException 
	{
		if(debug) System.out.println("Start tag " + xmlParser.getName());
		xmlParser.require(XmlPullParser.START_TAG, ns, t);
	}

	//legge il tag END_TAG
	void matchEnd(String t) throws XmlPullParserException, IOException 
	{
		if(debug) System.out.println("END tag " + xmlParser.getName());
		xmlParser.require(XmlPullParser.END_TAG, ns, t);
	}
	
	public Feed rss() throws XmlPullParserException, IOException
	{
		matchStart(Tag.rss);
		Feed feed = new Feed();
		move("rss1");
		while (eventType != XmlPullParser.END_TAG) { // fino a che non raggiungo l'END_TAG
			if (xmlParser.getEventType() != XmlPullParser.START_TAG) 
			{ 
				move("rss2");  // se sono ancota nel tag rss vado avanti
				continue;
			}
			String name = xmlParser.getName();
			if (name.equals(Tag.channel)) 
			{ // sennò mi chiedo se ho raggiundo il tag successivo
				feed = channel();
			} else 
			{ // in caso contrario vado avanti
				skip();
			}
			move("rss3");
		}
		matchEnd(Tag.rss);		
		return feed;
	}
	
	private Feed channel() throws XmlPullParserException, IOException 
	{
		matchStart(Tag.channel);
		Feed feed = new Feed();
		move("channel1");
		while (eventType != XmlPullParser.END_TAG) 
		{ // fino a che non raggiungo l'END_TAG
			if (xmlParser.getEventType() != XmlPullParser.START_TAG) 
			{ 
				move("channel2");  // se sono ancota nel tag channel vado avanti
				continue;
			}
			String name = xmlParser.getName();
			if (name.equals(Tag.title)) 
			{
				String title = title();
				feed.setTitle(title);
			}
			else if (name.equals(Tag.link)) 
			{
				String link = link();
				feed.setTitle(link);
			}
			else if(name.equals(Tag.description)) 
			{
				String description = description();
				feed.setDescription(description);
			}
			else if(name.equals(Tag.pubDate)) 
			{
				String pubDate = pubDate();
				feed.setPubDate(pubDate);
			} 
			else if(name.equals(Tag.item)) 
			{
				FeedMessage feedMessage = item();
				feed.addFeed(feedMessage);
			} 
			else 
			{
				skip();
			}
			move("channel3");
		}
		matchEnd(Tag.channel);
		return feed;		
	}
	
	private FeedMessage item() throws XmlPullParserException, IOException 
	{
		matchStart(Tag.item);
		FeedMessage feedMessage = new FeedMessage();
		move("item1");
		while (eventType != XmlPullParser.END_TAG) 
		{ // fino a che non raggiungo l'END_TAG
			if (xmlParser.getEventType() != XmlPullParser.START_TAG) 
			{ 
				move("item2");  // se sono ancota nel tag item vado avanti
				continue;
			}
			String name = xmlParser.getName();
			if (name.equals(Tag.title)) 
			{
				String title = title();
				feedMessage.setTitle(title);
			}
			else if (name.equals(Tag.link)) 
			{
				String link = link();
				feedMessage.setLink(link);
			}
			else if(name.equals(Tag.description)) 
			{
				String description = description();
				feedMessage.setDescription(description);
			}
			else if(name.equals(Tag.pubDate)) 
			{
				String pubDate = pubDate();
				feedMessage.setPubDate(pubDate);
			} 
			else if(name.equals(Tag.guid)) 
			{
				String guid = guid();
				feedMessage.setGuid(guid);
			}
			else 
			{
				skip();
			}
			move("item3");
		}
		matchEnd(Tag.item);		
		return feedMessage;
	}

	private String title() throws XmlPullParserException, IOException 
	{ 
		matchStart(Tag.title);
		String title = readText();
		move("title1");
		matchEnd(Tag.title);
		return title;
	}
	
	private String link() throws XmlPullParserException, IOException 
	{
		matchStart(Tag.link);
		String link = readText();
		move("link1");
		matchEnd(Tag.link);
		return link;
	}
	
	private String description() throws XmlPullParserException, IOException 
	{
		matchStart(Tag.description);
		String description = readText();
		move("description1");
		matchEnd(Tag.description);
		return description;
	}
	
	private String pubDate() throws XmlPullParserException, IOException 
	{
		matchStart(Tag.pubDate);
		String pubDate = readText();
		move("pubDate1");
		matchEnd(Tag.pubDate);
		return pubDate;
	}
	
	private String guid() throws XmlPullParserException, IOException 
	{
		matchStart(Tag.guid);
		String guid = readText();
		move("guid1");
		matchEnd(Tag.guid);
		return guid;
	}
	
	/*
	 * permette di saltare tutti i token non di mio interesse
	 * consuma un token START_TAG e tutto il contenuto fino al successivo token END_TAG
	 * */
	private void skip() throws XmlPullParserException, IOException 
	{
		if (eventType != XmlPullParser.START_TAG) throw new IllegalStateException();
		int depth = 1;
		move("skip");
		while (depth != 0) 
		{
			switch (eventType) 
			{
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
			move("skip");
		}
	}
	
	/*
	 * restituisce, se presente, il testo associato al tag corrente
	 * */
	private String readText() throws XmlPullParserException, IOException
	{
		String result = "";
		if (xmlParser.next() == XmlPullParser.TEXT)
		{
			result = xmlParser.getText();
		}
		return result;
	}

	public static void main(String[] args) throws XmlPullParserException, IOException 
	{	
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xmlParser = factory.newPullParser();	
	//possibili url da cui prendere rss diverse
		//URL url1 = new URL("http://informatica.i-learn.unito.it/rss/file.php/1/1/forum/1/rss.xml");
		URL url2 = new URL("http://www.educ.di.unito.it/EduFeed/rssNews_20.xml");
	//creo e setto la connessione per lo scaricamento del file xml
		HttpURLConnection conn = (HttpURLConnection)url2.openConnection();
		conn.setReadTimeout(10000); // millisecondi
		conn.setConnectTimeout(15000); //millisecondi
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		InputStream xmlStream = conn.getInputStream();
		xmlParser.setInput(xmlStream, null);
	//richiamo il costruttore di questa classe a cui passo  l'XMLPullParser
		RSSFeedParser parser = new RSSFeedParser(xmlParser);
	//assegno ad un nuovo feed la computazione del metodo rss di questa classe
		Feed feed = parser.rss();
		xmlStream.close();
		FeedToHTML f2h = new FeedToHTML("Feed I-learn");
		feed.accept(f2h);
		System.out.println(f2h.getHTML());
	//creo un file html con l'html che ho ricavato dai feed
		File outputFile = new File("RSSparser.html");
		FileWriter out = new FileWriter(outputFile);
		out.write(f2h.getHTML());		
		out.close();
	}
	
}
