package tk.maurocerbai.iumlabv4_fragment;

public class Ipsum 
{
	//public final static String RSS_NEWSDIP_TITLE="Avvisi dal Dip. Informatica";
	//public final static String RSS_NEWSDIP="http://www.unito.it/unitoWAR/rss.feed?channel=/BEA%20Repository/188058&lang=it";
	//public final static String RSS_AVVISIDIP_TITLE="Eventi dal Dip. Informatica";
	//public final static String RSS_AVVISIDIP="http://www.unito.it/unitoWAR/rss.feed?channel=/BEA%20Repository/188176&lang=it";
	//public final static String RSS_ICTBLOG_TITLE="ICT Dipinfo Blog";
	//public final static String RSS_ICTBLOG="http://blogs.di.unito.it/feed";
	public final static String RSS_NEWS_TITLE="Eventi e news da Educ";
	public final static String RSS_NEWS="http://www.educ.di.unito.it/EduFeed/rssNews_20.xml";
	public final static String RSS_STAGE_TITLE="Proposte di stage da Educ";
	public final static String RSS_STAGE="http://www.educ.di.unito.it/EduFeed/rssTesi_20.xml";
	//public final static String RSS_RISULTATI_TITLE="Risultati di esami da Educ";
	//public final static String RSS_RISULTATI="http://www.educ.di.unito.it/EduFeed/rssVoti_20.xml";
	public final static String RSS_ILEARN_TITLE="News da I-Learn";
	public final static String RSS_ILEARN="http://informatica.i-learn.unito.it/rss/file.php/1/1/forum/1/rss.xml";
	//public final static String RSS_AVVISIUNITO_TITLE="Avvisi UniTo";
	//public final static String RSS_AVVISIUNITO="http://www.unito.it/unitoWAR/rss.feed?channel=/BEA%20Repository/188012&lang=it";
	//public final static String RSS_EVENTIUNITO_TITLE="Eventi UniTo";
	//public final static String RSS_EVENTIUNITO="http://www.unito.it/unitoWAR/rss.feed?channel=/BEA%20Repository/188130&lang=it";
	
	public final static String [] Links ={
		/*RSS_NEWSDIP,RSS_AVVISIDIP,RSS_ICTBLOG,*/RSS_NEWS,RSS_STAGE,/*RSS_RISULTATI,*/RSS_ILEARN/*,RSS_AVVISIUNITO,RSS_EVENTIUNITO*/
	};
	
	public final static String [] Headlines ={
		/*RSS_NEWSDIP_TITLE,RSS_AVVISIDIP_TITLE,RSS_ICTBLOG_TITLE,*/RSS_NEWS_TITLE,RSS_STAGE_TITLE,/*RSS_RISULTATI_TITLE,*/RSS_ILEARN_TITLE/*,RSS_AVVISIUNITO_TITLE,RSS_EVENTIUNITO_TITLE*/
	};
}
