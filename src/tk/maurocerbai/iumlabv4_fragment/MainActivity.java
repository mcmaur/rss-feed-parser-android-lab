package tk.maurocerbai.iumlabv4_fragment;

import tk.maurocerbai.iumlabv4_fragment.HeadlinesFragment.OnHeadLineSelectedListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements OnHeadLineSelectedListener
{
	public static final String WIFI="Wi-Fi";
	public static final String ANY="Any";
	public static boolean wifiConnected=false;
	public static boolean mobileConnected=false;
	public static boolean refreshDisplay=false;
	public static String sPref=null;
	private ArticleFragment articleFrag;
	
	private NetworkReceiver receiver = new NetworkReceiver();

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		IntentFilter filter = new IntentFilter (ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkReceiver();
		this.registerReceiver(receiver, filter);
		setContentView(R.layout.new_articles);
		if(findViewById(R.id.fragment_container)!=null)
		{//se usiamo il layout con il fragment_container allora siamo nel caso normale
			if(savedInstanceState!=null)
			{
				return;
			}
			HeadlinesFragment firstFragment = new HeadlinesFragment();
			firstFragment.setArguments(getIntent().getExtras());
			getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,firstFragment).commit();
		}
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		if(receiver != null)
		{
			this.unregisterReceiver(receiver);
		}
	}

	@Override
	public void onStart()
	{
		super.onStart();
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		sPref = sharedPrefs.getString("listPref", "Wi-Fi");
		updateConnectedFlags();
	}

	public void updateConnectedFlags()
	{
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activateInfo = connMgr.getActiveNetworkInfo();
		if(activateInfo != null && activateInfo.isConnected())
		{
			wifiConnected = activateInfo.getType() == ConnectivityManager.TYPE_WIFI;
			mobileConnected = activateInfo.getType() == ConnectivityManager.TYPE_MOBILE;
		}
		else
		{
			wifiConnected=false;
			mobileConnected=false;
		}
	}

	public static boolean isConnected()
	{
		return (MainActivity.sPref.equals(MainActivity.ANY)) && (MainActivity.wifiConnected || MainActivity.mobileConnected) || ((MainActivity.sPref.equals(MainActivity.WIFI)) && (MainActivity.wifiConnected));
	}

	public void onArticleSelected(int position)
	{
		articleFrag = (ArticleFragment) getSupportFragmentManager().findFragmentById(R.id.article_fragment);
		if(articleFrag!=null)
		{//siamo nel layout large perchè il fragment dell'articolo è già presente
			articleFrag.updateArticleView(position);
		}
		else
		{//layout normale quindi necessaria transazione
			ArticleFragment newFragment = new ArticleFragment();
			Bundle args = new Bundle();
			args.putInt(ArticleFragment.ARG_POSITION,position);
			newFragment.setArguments(args);
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.fragment_container, newFragment);
			transaction.addToBackStack(null);
			transaction.commit();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mainmenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected (MenuItem item)
	{
		switch(item.getItemId())
		{
			case R.id.settings:
				Intent settingsActivity = new Intent (getBaseContext(),SettingActivity.class);
				startActivity(settingsActivity);
				return true;
			case R.id.refresh:
				if(articleFrag!=null)
					articleFrag.updateArticleView(articleFrag.mCurrentPosition);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	public class NetworkReceiver extends BroadcastReceiver
	{
		public void onReceive(Context arg0, Intent arg1) 
		{
			updateConnectedFlags();
			ConnectivityManager conn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkinfo = conn.getActiveNetworkInfo();
			if(WIFI.equals(sPref) && networkinfo!=null && networkinfo.getType()==ConnectivityManager.TYPE_WIFI)
			{
				refreshDisplay=true;
				Toast.makeText(getBaseContext(), R.string.wifi_connected, Toast.LENGTH_SHORT).show();
			}
			else if(ANY.equals(sPref) && networkinfo!=null)
			{
				refreshDisplay = false;
			}
			else
			{
				refreshDisplay = false;
				Toast.makeText(getBaseContext(), R.string.lost_connection, Toast.LENGTH_SHORT).show();
			}
		}
	}
	
}
